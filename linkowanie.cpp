
#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdio>
#include <random>



using namespace std;



class probe
{

private:

	int v;
	probe *nastepny;
public:

	probe(int value)
	{

		this->v = value;
		this->nastepny = NULL;
	}


	int getV()
	{
		return v;
	}

	void setV(int wartosc)
	{
		this->v = wartosc;
	}

	probe *getN()
	{
		return nastepny;
	}

	void setN(probe *n)
	{
		this->nastepny = n;
	}
};


class hashTab
{

private:

	probe **tab;
	int n;

public:


	hashTab(int size)
	{
		tab = new probe*[size];
		for (int i = 0; i < size; i++)
			tab[i] = NULL;
		n = size;
	}


	int hashFun(int v)
	{
		return v%n;
	}


	int get(int v)
	{
		int hash = hashFun(v);
		if (tab[hash] == NULL)
			return -1;
		else
		{
			probe *entry = tab[hash];
			while (entry != NULL && entry->getV() != v)
				entry = entry->getN();
			if (entry == NULL)
				return -1;
			else
				return entry->getV();
		}
	}

	void put(int v)
	{
		int hash = hashFun(v);
		if (tab[hash] == NULL)
			tab[hash] = new probe(v);
		else
		{
			probe *entry = tab[hash];
			while (entry->getN() != NULL)
				entry = entry->getN();
			if (entry->getV() == v)
				entry->setV(v);
			else
				entry->setN(new probe(v));
		}
	}

	void re(int v)
	{

		int hash = hashFun(v);
		if (tab[hash] != NULL)
		{
			probe *poprzedni = NULL;
			probe *wej = tab[hash];

			while (wej->getN() != NULL && wej->getV() != v)
			{
				poprzedni = wej;
				wej = wej->getN();
			}

			if (wej->getV() == v)
			{
				if (poprzedni == NULL)
				{
					probe *nastepny = wej->getN();
					delete wej;
					tab[hash] = nastepny;
				}
				else
				{
					probe *nastepny = wej->getN();
					delete wej;
					poprzedni->setN(nastepny);
				}
			}
		}
	}

	void view()
	{
		probe *x;
		for (int i = 0; i < n; i++)
		{
			if (tab[i] == NULL)
			{
				cout << "NULL\n";
				cout << "---\n";
			}
			else
			{
				cout << tab[i]->getV() << "\n";
				x = tab[i];
				while (x->getN() != NULL)
				{
					cout << "Dla klucza:" << hashFun(tab[i]->getV()) << ", rowniez ta wartosc:\n";
					cout << x->getN()->getV() << "\n";
					x = x->getN();
				}
				cout << "---\n";
			}

		}
		cout << "\n";
	}

	~hashTab()
	{
		for (int i = 0; i < n; i++)
		{
			if (tab[i] != NULL)
			{
				probe *prevEntry = NULL;
				probe *entry = tab[i];

				while (entry != NULL)
				{
					prevEntry = entry;
					entry = entry->getN();
					delete prevEntry;
				}
			}
		}

		delete[] tab;
	}
};

bool czyPierwsza(int w)
{
	bool a = false;
	for (int i = 2; i*i <= 100000; i++)
	{
		if (w == 1)
		{
			a = true;
			break;
		}

		if (w == 2)
		{
			a = false;
			break;
		}

		if ((w%i == 0) && (w != i))
		{
			a = true;
			break;
		}

	}
	if (a != true)
		return true;
	else
		return false;

}

int main()
{
	int n;
	cout << "Podaj wielkosc tablicy, musi to byc liczba pierwsza:\n";
	do
	{
		cin >> n;
		if (czyPierwsza(n) == true)
			cout << "Zgadza sie, to liczba pierwsza\n";
		else
		{
			cout << "To nie jest liczba pierwsza, podaj jeszcze raz\n";
		}
	} while (czyPierwsza(n) == false);





	hashTab hash(n);
	int v, g, liczba;
	int wybor;


	do
	{

		cout << "1.Dodaj element\n";
		cout << "2.Szukaj podajac wartosc\n";
		cout << "3.Usun podajac wartosc\n";
		cout << "4.Wyswietl\n";
		cout << "5.Koniec\n";
		cout << "Podaj wybor: ";

		cin >> wybor;

		cout << "\n";

		switch (wybor)
		{
		case 1:
			cout << "Ile chcesz dodac elementow" << endl;
			cin >> g;


			for (int i = 0; i < g; i++)
			{
				liczba = (rand() % 80) + 1;
				hash.put(liczba);
				cout << "wstawiono liczbe : " << liczba << endl;
			}
			break;
			/*cout << "Podaj wartosc do dodania: ";
			cin >> v;
			hash.put(v);
			break; */
		case 2:
			cout << "Podaj szukana wartosc: ";
			cin >> v;
			if (hash.get(v) == -1)
			{
				cout << "Nie znaleziono elementu o wartosci: " << v << endl;
				continue;
			}
			else
			{
				cout << "Element o kluczu" << v%n << " : ";
				cout << hash.get(v) << endl;
			}
			break;
		case 3:
			cout << "Podaj wartosc usuwanego elementu: ";
			cin >> v;
			hash.re(v);
			break;
		case 4:
			hash.view();
			break;
		case 5:
			break;
		default:
			cout << "\nBledny wybor\n";
		}
	} while (wybor != 5);



	system("PAUSE");
	return 0;
}


