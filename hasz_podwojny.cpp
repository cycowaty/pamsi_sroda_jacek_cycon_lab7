
#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdio>
#include <random>
//#include <Windows.h>

using namespace std;

bool czyPierwsza(int);


class probe{
private:
	int v;
	probe *nastepny;
public:
	probe(int wartosc) {

		this->v = wartosc;
		this->nastepny = NULL;
	}


	int getV() {
		return v;
	}

	void setV(int wartosc) {
		this->v = wartosc;
	}

};


class hashTab {
private:
	probe **tab;
	int n, Q;
public:

	hashTab(int size)
	{
		tab = new probe*[size];
		for (int i = 0; i < size; i++)
			tab[i] = NULL;
		n = size;
	}

	int hashFun(int v)
	{
		return v%n;
	}

	int hashFun2(int v)
	{
		return (5 - (v % 5));
	}

	int get(int v)
	{

		int hash = hashFun(v);
		int step = hashFun2(v);


		while (tab[hash] != NULL)
		{
			if (tab[hash]->getV() == v)
				return tab[hash]->getV();
			hash += step;
			hash %= n;
		}

		return NULL;

	}

	void put(int v)
	{
		int hash = hashFun(v);
		int step = hashFun2(v);
		int i=0;

		while (tab[hash] != NULL && tab[hash]->getV() != -1)
		{
			hash += step;
			hash %= n;
			i++;

			cout << "uzyto n:" <<step<<endl;
		}
		//cout << " \n";
		cout << "ilosc probek " << i;
		cout << "\n";
		tab[hash] = new probe(v);


	}

	void re(int v)
	{
		int hash = hashFun(v);
		int step = hashFun2(v);


		while (tab[hash] != NULL)
		{
			if (tab[hash]->getV() == v)
				tab[hash] = NULL;
			hash += step;
			hash %= n;
		}
		cout << "Nie ma takiego elementu!\n";
		return;

	}

	void view()
	{
		probe *x;
		for (int i = 0; i < n; i++)
		{
			if (tab[i] == NULL)
			{
				cout << "NULL\n";
				cout << "---\n";
			}
			else
			{
				cout<< tab[i]->getV() << "\n";

				cout << "---\n";
			}

		}
		cout << "\n";
	}

	~hashTab()
	{
		for (int i = 0; i < n; i++)
		{
			if (tab[i] != NULL)
			{
				tab[i] = NULL;
			}
		}
		delete[] tab;
	}
};

bool czyPierwsza(int w)
{
	bool a = false;
	for (int i = 2; i*i <= 100000; i++)
	{
		if (w == 1)
		{
			a = true;
			break;
		}

		if (w == 2)
		{
			a = false;
			break;
		}

		if ((w%i == 0) && (w != i))
		{
			a = true;
			break;
		}

	}
	if (a != true)
		return true;
	else
		return false;

}


int main()
{
	//srand(time(NULL));
	int n, Q;

	cout << "Podaj wielkosc tablicy, musi to byc liczba pierwsza:\n";
	do
	{
		cin >> n;
		if (czyPierwsza(n) == true)
			cout << "Zgadza sie, to liczba pierwsza\n";
		else
		{
			cout << "To nie jest liczba pierwsza, podaj jeszcze raz\n";
		}
	} while (czyPierwsza(n) == false);

	cout << "podaj wartość Q" << endl;
	do{
	cin >> Q;
	if (czyPierwsza(Q) == true && Q<n)
		cout << "Zgadza sie, to liczba pierwsza i mniejsza od n\n";
	else
	{
		cout << "Nieprawidłowa liczba, podaj jeszcze raz\n";
	}
} while (czyPierwsza(Q) == false || Q>=n);



	hashTab hash(n);
	int v, g, liczba;
	int wybor;


	do
	{
		cout << "1.Dodaj element\n";
		cout << "2.Szukaj podajac wartosc\n";
		cout << "3.Usun podajac wartosc\n";
		cout << "4.Wyswietl\n";
		cout << "5.Koniec\n";
		cout << "Podaj wybor: ";

		cin >> wybor;

		cout << "\n";

		switch (wybor)
		{
		case 1:
			cout << "Ile chcesz dodac elementow" << endl;
			cin >> g;
			if (g > n)

			{
				cout << "Podaj liczbe =<n";
				cout << "\n";
			}
			else
			for (int i = 0; i < g; i++)
			{
				liczba = (rand() % 80) + 1;
				hash.put(liczba);
				cout << "wstawiono liczbe : " << liczba << endl;
				//cout <<   << endl;
			}


			break;
			/*cout << "Podaj wartosc do dodania: ";
			cin >> v;
			hash.put(v);
			break; */
		case 2:
			cout << "Podaj szukana wartosc: ";
			cin >> v;
			if (hash.get(v) == -1)
			{
				cout << "Nie znaleziono elementu: " << v << endl;
				continue;
			}
			else
			{
				cout << "Znaleziony element" << " : ";
				cout << hash.get(v) << "\n";
			}
			break;
		case 3:
			cout << "Podaj wartosc usuwanego elementu: ";
			cin >> v;
			hash.re(v);
			break;
		case 4:
			hash.view();
			break;
		case 5:
			break;

		default:
			cout << "\nBledny wybor\n";
		}
	} while (wybor != 5);



	system("PAUSE");
	return 0;
}


